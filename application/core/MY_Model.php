<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model
{
	function return_select($result){
		if($result->num_rows() > 0)
			return $result;
		else
			return false;
	}
	
	function return_select_first_row($result){
		if($result->num_rows() > 0)
			return $result->row();
		else
			return false;
	}
}