<?php

class Modul_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function get_free_modul($level){
    	$this->db->select('*')
    				->from('modul')
                    ->where('gratis', '1')
                    ->where('levelnya <=', $level);
    	$query = $this->db->get();

    	return $this->return_select($query);
    }

    function count_modul($level){
        $this->db->select('count(*) as total_modul', false)
                    ->from('modul')
                    ->where('levelnya', $level);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_modul_by_level($level){
        $this->db->select('*')
                    ->from('modul')
                    ->where('levelnya', $level)
                    ->order_by('idmatkul, no_urut');
        $query = $this->db->get();

        return $this->return_select($query);
    }

    function get_modul_by_matkul($matkul_id){
        $this->db->select('*')
                    ->from('modul')
                    ->where('idmatkul', $matkul_id)
                    ->order_by('no_urut');
        $query = $this->db->get();

        return $this->return_select($query);
    }

    function get_free_modul_by_matkul($matkul_id){
        $this->db->select('*')
                    ->from('modul')
                    ->where('idmatkul', $matkul_id)
                    ->where('gratis', '1')
                    ->order_by('no_urut');
        $query = $this->db->get();

        return $this->return_select($query);
    }

    function get_modul_by_id($id){
        $this->db->select('mo.*, u.nama, ma.namamatakuliah')
                    ->from('modul mo')
                    ->join('matakuliah ma', 'mo.idmatkul = ma.idmatkul')
                    ->join('user u', 'ma.iduser = u.iduser')
                    ->where('mo.idmodul', $id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_prev_modul($id, $level){
        $this->db->select('mo.*, u.nama, ma.namamatakuliah')
                    ->from('modul mo')
                    ->join('matakuliah ma', 'mo.idmatkul = ma.idmatkul')
                    ->join('user u', 'ma.iduser = u.iduser')
                    ->where('mo.idmodul <', $id)
                    ->where('mo.levelnya', $level)
                    ->order_by('mo.idmatkul, mo.no_urut');
        $query = $this->db->get();

        if($query->num_rows() > 0)
            return $query->last_row();
        else
            return false;
    }

    function get_next_modul($id, $level){
        $this->db->select('mo.*, u.nama, ma.namamatakuliah')
                    ->from('modul mo')
                    ->join('matakuliah ma', 'mo.idmatkul = ma.idmatkul')
                    ->join('user u', 'ma.iduser = u.iduser')
                    ->where('mo.idmodul >', $id)
                    ->where('mo.levelnya', $level)
                    ->order_by('mo.idmatkul, mo.no_urut')
                    ->limit(1);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_max_score($user_id, $modul_id){
        $this->db->select('max(score) as max_score', false)
                    ->from('scoremahasiswa')
                    ->where('iduser', $user_id)
                    ->where('idmodul', $modul_id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_history_learning($user_id, $modul_id){
        $this->db->select('*')
                    ->from('historylearning')
                    ->where('iduser', $user_id)
                    ->where('idmodul', $modul_id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

}