<?php

class Appconfig_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function get_config_value_by_key($key){
    	$this->db->select('*')
    				->from('app_configs')
    				->where('parameter_key', $key);
    	$query = $this->db->get();
        $value = $query->row()->parameter_value;
    	return $value;
    }

}