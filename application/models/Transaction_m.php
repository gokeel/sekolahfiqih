<?php

class Transaction_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }
	
	function get_transaction_by_id($id){
		$this->db->select('*')
					->from('transaction')
					->where('transaction_id', $id);

		$query = $this->db->get();
		
		return $this->return_select_first_row($query);
	}

	function get_transaction_by_number($number){
		$this->db->select('*')
					->from('transaction')
					->where('transaction_number', $id);

		$query = $this->db->get();
		
		return $this->return_select_first_row($query);
	}
	
}