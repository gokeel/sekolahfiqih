<?php

class Fcm_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function check_token_exist($token){
    	$this->db->select('*')
    				->from('fcm_tokens')
    				->where('token', $token);
    	$query = $this->db->get();

    	if($query->num_rows() > 0)
            return true;
        else
            return false;
    }

}