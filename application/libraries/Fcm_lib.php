<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Fcm_lib {
	private $ci;
	private $API_SERVER_KEY = 'AAAAZrutmh8:APA91bEB-Zzh7iOV8wUjl7vi0pba8m1tMTEeHoZVHd-q-6wmfAbTb7kKUNTeCiC-fjznPJXOC5qoNeTaYRdQxbhxg8H7tP7cU_dL8gjct1KKTkBcEgURheNVjZfD0N-HDJ_7wSgQNnd-';
	
	public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function sendPushNotificationToFCMSever($token, $title, $message, $notifyID=null) {
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
 
        $fields = array(
            'registration_ids' => $token,
            'priority' => 10,
            'notification' => array('title' => $title, 'body' =>  $message ,'sound'=>'Default','image'=>'Notification Image' ),
        );
        $headers = array(
            'Authorization:key=' . $this->API_SERVER_KEY,
            'Content-Type:application/json'
        );  
         
        // Open connection  
        $ch = curl_init(); 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post   
        $result = curl_exec($ch); 
        // Close connection      
        curl_close($ch);
        return $result;
    }

    function sendFCM($token, $message, $message_info='', $type ='') {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
                'registration_ids' => array (
                        $token
                ),
                'data' => array (
                        "message" => $message,
                        'message_info' => $message_info,
                ),                
                'priority' => 'high',
                'notification' => array(
                            'title' => $message['title'],
                            'body' => $message['body'],                            
                ),
        );
        $fields = json_encode ( $fields );

        $headers = array (
                'Authorization: key=' . $this->API_SERVER_KEY,
                'Content-Type: application/json'
        );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec ( $ch );
        curl_close ( $ch );
    }
}
