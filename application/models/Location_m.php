<?php

class Location_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }
	
	function get_province($filter_array=null){
		$this->db->select('*');
		$this->db->from('provinces');
		
		if($filter_array <> null)
			$this->db->where($filter_array);
		$this->db->order_by('province_name');
		$query = $this->db->get();
		
		return $this->return_select($query);
	}

	function get_city($filter_array=null){
		$this->db->select('c.*, p.province_name');
		$this->db->from('cities c');
		$this->db->join('provinces p', 'c.province_id = p.province_id');
		
		if($filter_array <> null)
			$this->db->where($filter_array);
		$this->db->order_by('c.city_name');
		$query = $this->db->get();
		
		return $this->return_select($query);
	}
	
}