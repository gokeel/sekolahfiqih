<?php

class User_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function get_user_list($filter=null){
		$this->db->select('*');
		$this->db->from('user');
		
		if($filter <> null)
			$this->db->where($filter);
		$query = $this->db->get();
		
		return $this->return_select($query);
	}

	function get_user_by_id($userid){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('iduser', $userid);
		
		$query = $this->db->get();

		return $this->return_select_first_row($query);
	}

	function get_user_by_email($email){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email', $email);
		
		$query = $this->db->get();

		return $this->return_select_first_row($query);
	}

	function check_user_id_exist($userid){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('iduser', $userid);

		$query = $this->db->get();

		if($query->num_rows() > 0)
			return true;
		else return false;
	}

	function check_email_exist($email){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email', $email);

		$query = $this->db->get();

		if($query->num_rows() > 0)
			return true;
		else return false;
	}

	function check_user_for_login($email, $password, $key){
		$role = array('dosen', 'mahasiswa');
		$this->db->select('*')
					->from('user')
					->where("email = '".$email."' AND password = md5(CONCAT('".$key."',md5('".$password."'),'".$key."'))")
					->where_in('role', $role);
		// $this->db->where("email_login = '".$key_value."' AND CAST(AES_DECRYPT(password_app, '".$key."') as CHAR(50)) = '".$password."'");
		$query = $this->db->get();

		return $this->return_select_first_row($query);
	}
        
    function get_salt($email) {
        $this->db->select('salt');
        $this->db->where('email_login', $email);
        $res = $this->db->get('user');
        return $res->row('salt');
    }
        
	function update_password($email, $new_pass){
		$this->db->where('email_login', $email);
		$this->db->update('user', array('password' => md5($new_pass)));
		if($this->db->affected_rows() > 0)
			return true;
		else{
			$error = $this->db->error();
			if($error['code']<>0)
				return $error['message'];
		}
	}

	function get_reset_password($user_id){
		$this->db->select('password_generated');
		$this->db->from('request_reset_password');
		$this->db->where('iduser', $user_id);

		$query = $this->db->get();

		if($query->num_rows() > 0)
			return $query->row();
		else return false;
	}

	function increment_total_user_viewed($user_id){
		$query = $this->db->query("UPDATE user_profiles SET total_viewed = total_viewed + 1 WHERE user_id = '".$user_id."'");

		return true;
	}

	function is_admin_granted($user_id){
		$this->db->select('*')
					->from('user_admin_grants')
					->where('iduser', $user_id)
					->where('is_active', 'y');

		$query = $this->db->get();

		if($query->num_rows() > 0)
			return true;
		else return false;
	}

	function get_token($user_id){
		$this->db->select('*')
					->from('user_tokens')
					->where('user_id', $user_id);

		$query = $this->db->get();

		return $this->return_select_first_row($query);
	}

	function check_password_by_id($user_id, $password, $key){
		$this->db->select('*')
					->from('user')
					->where("iduser = '".$user_id."' AND password = md5(CONCAT('".$key."',md5('".$password."'),'".$key."'))");
		// $this->db->where("email_login = '".$key_value."' AND CAST(AES_DECRYPT(password_app, '".$key."') as CHAR(50)) = '".$password."'");
		$query = $this->db->get();

		return $this->return_select_first_row($query);
	}

	function check_email_token_reset_pass($user_id, $token){
		$this->db->select('*')
					->from('reset_password')
					->where('user_id', $user_id)
					->where('token', $token)
					->where('used', '0');
		$query = $this->db->get();

		return $this->return_select_first_row($query);
	}
}