<?php

class Dosen_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function get_all_dosen(){
    	$this->db->select('*')
    				->from('dosen')
    				->order_by('nama');
    	$query = $this->db->get();

    	return $this->return_select($query);
    }

    function get_dosen_by_id($id){
        $this->db->select('*')
                    ->from('dosen')
                    ->where('id_dosen', $id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_dosen_by_user_id($user_id){
        $this->db->select('*')
                    ->from('dosen')
                    ->where('user_id', $user_id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

}