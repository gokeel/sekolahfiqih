<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Token_lib {
	private $ci;
	
	public function __construct()
    {
        $this->ci =& get_instance();
    }

    function create($user_id, $token){
    	$new_token = $token;
    	
    	if($token==null)
    		$new_token = generate_random_string('letter', 50);

    	$data = array(
    		'id' => uniqid(),
    		'token' => $new_token,
    		'ip_address' => $this->ci->input->ip_address(),
    		'user_agent' => $this->ci->input->user_agent(),
    		'request_header' => json_encode($this->ci->input->request_headers()),
    		'user_id' => $user_id
    	);

    	// cek apakah data user id sudah masuk atau tidak
    	$this->ci->db->select('*')
    					->from('user_tokens')
    					->where('user_id', $user_id)
    					->where('token', $new_token);
    	$query = $this->ci->db->get();

    	if($query->num_rows() == 0)
	    	$insert = $this->ci->db->insert('user_tokens', $data);

    	return $new_token;
    }
	
	function is_active($token){
		$this->ci->db->select('*')
						->from('user_tokens')
						->where('token', $token);
						// ->where('DATEDIFF(now(), last_access_timestamp) <=', "14", false);
		$get = $this->ci->db->get();

		if($get->num_rows() > 0)
			return true;
		else
			return false;
	}

	function check_token_input($token){
		if($token == "")
			return false;
		
		else return $this->is_active($token);
	}

	function get_user_id($token){
		$this->ci->db->select('user_id')
						->from('user_tokens')
						->where('token', $token);
		$get = $this->ci->db->get();

		if($get->num_rows() > 0)
			return $get->row();
		else
			return false;
	}

	function remove($token){
    	$this->ci->db->delete('user_tokens', array('token' => $token));
    }
}
