<?php

class Ujian_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function get_soal($modul_id){
    	$this->db->select('*')
    				->from('soal')
                    ->where('idmodul', $modul_id)
                    ->order_by('urutan');
    	$query = $this->db->get();

    	return $this->return_select($query);
    }

    function count_modul($level){
        $this->db->select('count(*) as total_modul', false)
                    ->from('modul')
                    ->where('levelnya', $level);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_jawaban_benar($soal_id){
        $this->db->select('kunci')
                    ->from('soal')
                    ->where('idsoal', $soal_id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_score_mahasiswa_all($user_id, $modul_id){
        $this->db->select('*')
                    ->from('scoremahasiswa')
                    ->where('iduser', $user_id)
                    ->where('idmodul', $modul_id);
        $query = $this->db->get();

        return $this->return_select($query);
    }

    function get_score_mahasiswa_percobaan($user_id, $modul_id, $percobaan_ke){
        $this->db->select('*')
                    ->from('scoremahasiswa')
                    ->where('iduser', $user_id)
                    ->where('idmodul', $modul_id)
                    ->where('percobaanke', $percobaan_ke);
        $query = $this->db->get();

        if($query->num_rows() > 0)
            return $query->row()->score;
        else
            return '-';
    }
}