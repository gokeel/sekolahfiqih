<?php

class Account_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function get_my_level($user_id){
        $this->db->select('max(leveluser) current_level', false)
                    ->from('historylevel')
                    ->where('iduser', $user_id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_my_license($user_id){
        $this->db->select('*')
                    ->from('user_license')
                    ->where('user_id', $user_id)
                    ->where('start_date <=', date('Y-m-d'))
                    ->where('end_date >=', date('Y-m-d'))
                    ->order_by('license_id desc');
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_pro_license(){
        $this->db->select('*')
                    ->from('license')
                    ->like('license_id', 'PRO')
                    ->order_by('expiry_days');
        $query = $this->db->get();

        return $this->return_select($query);
    }

    function get_license_by_id($id){
        $this->db->select('*')
                    ->from('license')
                    ->where('license_id', $id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_notification($user_id){
        $this->db->select('*')
                    ->from('notifikasi')
                    ->where('receiver_id', $user_id)
                    ->order_by('id desc');
        $query = $this->db->get();

        return $this->return_select($query);
    }

}