<?php

class EmailTemplate_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function get_template_by_id($id){
    	$this->db->select('*')
    				->from('email_template')
    				->where('template_id', $id);
    	$query = $this->db->get();
        
    	return $this->return_select_first_row($query);
    }

}