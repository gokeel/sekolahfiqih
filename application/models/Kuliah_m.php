<?php

class Kuliah_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function count_level(){
    	$this->db->select('max(level) as max_level', false)
    				->from('matakuliah');
    	$query = $this->db->get();

    	return $this->return_select_first_row($query);
    }

    function count_matkul(){
        $this->db->select('count(*) as total_matkul', false)
                    ->from('matakuliah');
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function count_modul(){
        $this->db->select('count(*) as total_modul', false)
                    ->from('modul');
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function count_progress_modul($user_id, $level){
        $this->db->select('count(*) as progress_modul', false)
                    ->from('historylearning h')
                    ->join('modul m', 'h.IDMODUL = m.idmodul')
                    ->where('h.iduser', $user_id)
                    ->where('m.levelnya', $level);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_matkul_by_level($level){
        $this->db->select('m.*, u.nama')
                    ->from('matakuliah m')
                    ->join('user u', 'm.iduser = u.iduser')
                    ->where('m.level', $level)
                    ->order_by('m.no');
        $query = $this->db->get();

        return $this->return_select($query);
    }

    function get_free_matkul(){
        $this->db->select('m.*, u.nama')
                    ->from('matakuliah m')
                    ->join('user u', 'm.iduser = u.iduser')
                    ->where('m.level', '1')
                    ->order_by('m.no');
        $query = $this->db->get();

        return $this->return_select($query);
    }

    function get_matkul_by_id($id){
        $this->db->select('*')
                    ->from('matakuliah')
                    ->where('idmatkul', $id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

}