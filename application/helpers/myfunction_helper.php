<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('generate_random_string')) {

    function generate_random_string($type, $length) {
        $characters = '';
        if ($type == "letter")
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        else if ($type == "number")
            $characters = '0123456789';
        else if ($type == "mix")
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

function set_image_thumb_name($user_info) {
    $add_dot = '';
    if (substr($user_info->file_extension, 0, 1) <> ".")
        $add_dot = ".";
    $image_thumb = str_replace($user_info->file_extension, "", $user_info->file_name) . '_thumb' . $add_dot . $user_info->file_extension;

    return $image_thumb;
}

function get_month_string($number) {
    $month_array = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    );

    return $month_array[$number];
}

function download_file($path, $name) {
    if (is_file($path)) {
        $ci = & get_instance();
        // required for IE
        if (ini_get('zlib.output_compression')) {
            ini_set('zlib.output_compression', 'Off');
        }

        // get the file mime type using the file extension
        $ci->load->helper('file');

        $mime = get_mime_by_extension($path);

        // Build the headers to push out the file properly.
        header('Pragma: public');     // required
        header('Expires: 0');         // no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
        header('Cache-Control: private', false);
        header('Content-Type: ' . $mime);  // Add the mime type from Code igniter.
        header('Content-Disposition: attachment; filename="' . basename($name) . '"');  // Add the file name
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($path)); // provide file size
        header('Connection: close');
        readfile($path); // push it out
        exit();
    }
}

function generate_captcha(){
    $ci = & get_instance();
    //generate captcha
    $ci->load->helper('captcha');
    $captcha_data = array(
        'word' => rand(1000, 9999),
        'img_path' => 'assets/captcha/',
        'img_url' => base_url().'assets/captcha/',
        //'font_path' => './path/to/fonts/texb.ttf',
        'img_width' => '150',
        'img_height' => 30,
        'expiration' => 7200
        );

    $cap = create_captcha($captcha_data);
    $insert_cap = array(
        'captcha_time' => $cap['time'],
        'ip_address' => $ci->input->ip_address(),
        'word' => $cap['word']
    );
    $query = $ci->db->insert_string('captcha', $insert_cap);
    $ci->db->query($query);
    //echo $cap['image'];
    $captcha = $cap['image'];

    return $captcha;
}

function get_person_fullname($data){
    $fullname = $data->first_name;
    if($data->middle_name <> "" and $data->last_name <> "")
        $fullname .= ' '.$data->middle_name.' '.$data->last_name;
    else if($data->middle_name == "" and $data->last_name <> "")
        $fullname .= ' '.$data->last_name;
    else if($data->middle_name <> "" and $data->last_name == "")
        $fullname .= ' '.$data->middle_name;

    return $fullname;
}

function get_person_fullname_plus($data){
    $fullname = $data->first_name;
    if($data->middle_name <> "" and $data->last_name <> "")
        $fullname .= ' '.$data->middle_name.' '.$data->last_name;
    else if($data->middle_name == "" and $data->last_name <> "")
        $fullname .= ' '.$data->last_name;
    else if($data->middle_name <> "" and $data->last_name == "")
        $fullname .= ' '.$data->middle_name;

    if($data->initial <> "")
        $fullname = $data->initial.' '.$fullname;

    if($data->title <> "")
        $fullname .= ', '.$data->title;

    return $fullname;
}

function breakdown_fullname($fullname){
    $fn_split = explode(' ', $fullname);
    $name_array = array();
    if(sizeof($fn_split) == 1)
        $name_array = array(
            'first_name' => $fullname,
            'middle_name' => '',
            'last_name' => ''
        );
    else if(sizeof($fn_split) == 2)
        $name_array = array(
            'first_name' => $fn_split[0],
            'middle_name' => '',
            'last_name' => $fn_split[1]
        );
    else if(sizeof($fn_split) > 2){
        $middle_name = '';
        for($i=1; $i<=sizeof($fn_split) - 2; $i++)
            $middle_name .= $fn_split[$i].' ';
        $middle_name = rtrim($middle_name, ' ');
        $name_array = array(
            'first_name' => $fn_split[0],
            'middle_name' => $middle_name,
            'last_name' => $fn_split[sizeof($fn_split) - 1]
        );
    }
    
    return $name_array;
}

/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::                                                                         :*/
/*::  This routine calculates the distance between two points (given the     :*/
/*::  latitude/longitude of those points). It is being used to calculate     :*/
/*::  the distance between two locations using GeoDataSource(TM) Products    :*/
/*::                                                                         :*/
/*::  Definitions:                                                           :*/
/*::    South latitudes are negative, east longitudes are positive           :*/
/*::                                                                         :*/
/*::  Passed to function:                                                    :*/
/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
/*::    unit = the unit you desire for results                               :*/
/*::           where: 'M' is statute miles (default)                         :*/
/*::                  'K' is kilometers                                      :*/
/*::                  'N' is nautical miles                                  :*/
/*::  Worldwide cities and other features databases with latitude longitude  :*/
/*::  are available at https://www.geodatasource.com                          :*/
/*::                                                                         :*/
/*::  For enquiries, please contact sales@geodatasource.com                  :*/
/*::                                                                         :*/
/*::  Official Web site: https://www.geodatasource.com                        :*/
/*::                                                                         :*/
/*::         GeoDataSource.com (C) All Rights Reserved 2017                  :*/
/*::                                                                         :*/
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
function calculate_distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}