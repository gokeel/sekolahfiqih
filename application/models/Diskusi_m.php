<?php

class Diskusi_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }

    function get_pertanyaan_masuk($dosen_id, $terjawab='0'){
    	$this->db->select('hd.*, mo.judul, ma.namamatakuliah, ma.level')
    				->from('headerdiskusi hd')
                    ->join('modul mo', 'hd.IDMODUL = mo.idmodul')
                    ->join('matakuliah ma', 'mo.idmatkul = ma.idmatkul')
                    ->where('ma.iduser', $dosen_id)
                    ->where('hd.TERJAWAB', $terjawab)
                    ->order_by('hd.TGLBUAT desc');
    	$query = $this->db->get();

    	return $this->return_select($query);
    }

    function check_pertanyaan_terjawab($diskusi_id){
        $this->db->select('*')
                    ->from('detaildiskusi')
                    ->where('iddiskusi', $diskusi_id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function cek_siswa_sudah_tanya($siswa_id, $modul_id){
        $this->db->select('*')
                    ->from('headerdiskusi')
                    ->where('IDUSER', $siswa_id)
                    ->where('IDMODUL', $modul_id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

    function get_header_diskusi_by_id($id){
        $this->db->select('*')
                    ->from('headerdiskusi')
                    ->where('IDDISKUSI', $id);
        $query = $this->db->get();

        return $this->return_select_first_row($query);
    }

}