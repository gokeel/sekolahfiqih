<?php

class Person_m extends MY_Model {
	public function __construct() {
        parent::__construct();
    }
	
	function get_by_user_id($user_id){
		$this->db->select('*');
		$this->db->from('persons');
		$this->db->where('user_id', $user_id);
		
		$query = $this->db->get();
		
		return $this->return_select_first_row($query);
	}

	function get_by_person_id($person_id){
		$this->db->select('*');
		$this->db->from('persons');
		$this->db->where('person_id', $person_id);
		
		$query = $this->db->get();
		
		return $this->return_select_first_row($query);
	}

	function get_country_codes(){
		$this->db->select('*');
		$this->db->from('country_codes');
		$this->db->order_by('country');
		
		$query = $this->db->get();
		
		return $this->return_select($query);
	}
	
	function check_person_exist_in_table($table, $person_id) {
	    $this->db->select('*')
	               ->from($table)
	               ->where('person_id', $person_id);
	    
	    $query = $this->db->get();
	    if($query->num_rows() > 0)
	        return true;
	    else 
	        return false;
	}
}